<?php
namespace App\Controllers;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 *
 * @package CodeIgniter
 */

use CodeIgniter\Controller;

class BaseController extends Controller
{

	/**
	 * An array of helpers to be loaded automatically upon
	 * class instantiation. These helpers will be available
	 * to all other controllers that extend BaseController.
	 *
	 * @var array
	 */
	protected $helpers = ['form', 'access_helper'];
	/**
	 * Constructor.
	 */
	public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
	{
		// Do Not Edit This Line
		parent::initController($request, $response, $logger);

		//--------------------------------------------------------------------
		// Preload any models, libraries, etc, here.
		//--------------------------------------------------------------------
		// E.g.:
		$this->session = \Config\Services::session();
		$this->db = \Config\Database::connect();
	}

	public function check_access($module, $action){
		$user = $this->session->get('ss_user');
		if($user == null){
			header('Location: '.base_url().'/home/login');
			exit();
		}

		if(!has_permission($module, $action)){
			header('Location: '.base_url().'/home/login');
			exit();
		}
	}

	public function check_login(){
		$user = $this->session->get('ss_user');
		if($user == null){
			header('Location: '.base_url().'/home/login');
			exit();
		}
	}
	public function user_logout(){
		$this->session->destroy();
	}

}
