<?php namespace App\Controllers;

class Clients extends BaseController
{
	public function __construct() {
		$this->clientModel = model('App\Models\ClientModel');
		$this->vendorModel = model('App\Models\VendorModel');
	}
	public function index()
	{
		$this->check_access('clients', 'view');
		echo view('layout/header');
		$data['vendors'] = $this->vendorModel->get_options('');
		echo view('clients/index', $data);
		echo view('layout/footer');
	}

	public function get_clients_req(){
		$records = $this->clientModel->findAll();
        echo json_encode(array("aaData"=>$records));
	}


	public function save(){
		$data = $this->request->getPost();

		//Check for duplicate
		if($this->clientModel->email_exists($data)){
			return 'Email already exist, please use another one';
		}

		//Check assignment

		if($this->vendorModel->ip_assigned($data['vendor_id'])){
			return 'The selected ip is already assigned, please try another ip';
		}

		$res = $this->clientModel->save($data);
		if($res){
			//After saving, update vendor to assigned
			$data = array('is_assigned'=> 1, 'id' => $data['vendor_id']);
			$this->vendorModel->save($data);
			echo 'Success';
		}else{
			echo 'Unable to Save';
		}
	}

	public function get_edit($id){
		$client = $this->clientModel->where(array("id"=>$id))->findAll();
		if(sizeof($client) > 0)
		{
			$client = $client[0];
			$client->delivery_date =  date('Y-m-d', strtotime($client->delivery_date ) );
			$data['client'] = $client;
			$data['vendors'] = $this->vendorModel->get_options($client->vendor_id);
			echo view('clients/edit',$data);
		}
		else
		{
			echo 'Invalid Client';
		}
	}

	public function Delete($id)
	{
		$this->clientModel->delete($id);
		echo 'Success';
	}

}
