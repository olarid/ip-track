<?php namespace App\Controllers;

class Home extends BaseController
{
	public function index()
	{
		$this->check_login();
		echo view('layout/header');
		echo view('dashboard');
		echo view('layout/footer');
		// return view('dashboard');
	}

	public function updateprofile(){
		$this->check_access('dashboard','profile');
		echo view('layout/header');
		echo view('updateprofile');
		echo view('layout/footer');
	}

	public function login(){
		$this->userModel = model('App\Models\UserModel');
		$message = '';
		if($this->request->getMethod() == 'post'){
			$user = $this->userModel->where($this->request->getPost())->findAll();
			if(sizeof($user) > 0){
				//Set session and redirect
				$this->session->set('ss_user', $user[0]);
				//Get menus
				$this->session->set('menus', get_menus());
				return redirect()->to('/home');
			}else{
				$message = 'Invalid Username or password';
			}
		}
		$data['message'] = $message;
		echo view('authentication/login', $data);
	}

	public function logout(){
		$this->user_logout();
		return redirect()->to('/home/login');
	}

}
