<?php namespace App\Controllers;

class Locations extends BaseController
{
	public function __construct() {
		$this->locationModel = model('App\Models\LocationModel');
	}
	public function index()
	{
		$this->check_access('locations','view');
		echo view('layout/header');
		echo view('locations/index');
		echo view('layout/footer');
	}

	public function get_locations_req(){
		$locations = $this->locationModel->findAll();
        echo json_encode(array("aaData"=>$locations));
	}


	public function save(){
		$res = $this->locationModel->save($this->request->getPost());
		if($res){
			echo 'Success';
		}else{
			echo 'Unable to Save';
		}
	}

	public function get_edit($id){
		$location = $this->locationModel->where(array("id"=>$id))->findAll();
		if(sizeof($location) > 0)
		{
			$data['location'] = $location[0];
			echo view('locations/edit',$data);
		}
		else
		{
			echo 'Invalid Location';
		}
	}

	public function Delete($id)
	{
		$this->locationModel->delete($id);
		echo 'Success';
	}

}
