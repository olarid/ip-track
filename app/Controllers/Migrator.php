<?php namespace App\Controllers;

class Migrator extends BaseController
{
	public function index()
	{
		$migrate = \Config\Services::migrations();
		$seeder = \Config\Database::seeder();
		try
		{
			$migrate->latest();
			$seeder->call('mask_seeder');
			$seeder->call('region_seeder');
			$seeder->call('user_seeder');

			echo 'All done';
		}
		catch (\Throwable $e)
		{
			echo 'Not done';
		}
	}


}
