<?php namespace App\Controllers;

class Regions extends BaseController
{
	public function __construct() {
		$this->regionModel = model('App\Models\RegionModel');
	}
	public function index()
	{
		$this->check_access('regions','view');
		echo view('layout/header');
		echo view('regions/index');
		echo view('layout/footer');
	}

	public function get_regions_req(){
		$regions = $this->regionModel->findAll();
        echo json_encode(array("aaData"=>$regions));
	}


	public function save(){
		$res = $this->regionModel->save($this->request->getPost());
		if($res){
			echo 'Success';
		}else{
			echo 'Unable to Save';
		}
	}

	public function get_edit($id){
		$region = $this->regionModel->where(array("id"=>$id))->findAll();
		if(sizeof($region) > 0)
		{
			$data['region'] = $region[0];
			echo view('regions/edit',$data);
		}
		else
		{
			echo 'Invalid Region';
		}
	}

	public function Delete($id)
	{
		$this->regionModel->delete($id);
		echo 'Success';
	}

}
