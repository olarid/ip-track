<?php namespace App\Controllers;

class SubnetMasks extends BaseController
{
	public function __construct() {
		$this->maskModel = model('App\Models\SubnetMaskModel');
	}
	public function index()
	{
		$this->check_access('subnetmasks','view');
		echo view('layout/header');
		echo view('masks/index');
		echo view('layout/footer');
	}

	public function get_masks_req(){
		$data = $this->maskModel->findAll();
        echo json_encode(array("aaData"=>$data));
	}


	public function save(){
		$res = $this->maskModel->save($this->request->getPost());
		if($res){
			echo 'Success';
		}else{
			echo 'Unable to Save';
		}
	}

	public function get_edit($id){
		$records = $this->maskModel->where(array("id"=>$id))->findAll();
		if(sizeof($records) > 0)
		{
			$data['mask'] = $records[0];
			echo view('masks/edit',$data);
		}
		else
		{
			echo 'Invalid Data';
		}
	}

	public function Delete($id)
	{
		$this->maskModel->delete($id);
		echo 'Success';
	}

}
