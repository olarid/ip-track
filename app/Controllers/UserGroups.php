<?php namespace App\Controllers;

class UserGroups extends BaseController
{
	public function __construct() {
		$this->groupModel = model('App\Models\UserGroupModel');
	}
	public function index()
	{
		$this->check_access('usergroups', 'view');
		$data['permissions'] = get_permissions();
		echo view('layout/header');
		echo view('usergroups/index', $data);
		echo view('layout/footer');
	}

	public function get_groups_req(){
		$groups = $this->groupModel->findAll();
        echo json_encode(array("aaData"=>$groups));
	}


	public function save(){
		$data = $this->request->getPost();
		$res = $this->groupModel->save($data);
		if($res){
			$id='';

			//For edit, delete previous permissions
			if(isset($data['id'])){
				$id= $data['id'];
				$this->groupModel->purge_permissions($id);
			}else{
				//Get just inserted id
				$id = $this->groupModel->max_id();
			}

			//Insert group permissions
			$perms = array_filter($data, function($field){
				return $field == 'true';
			});

			$this->groupModel->add_permissions($id, $perms);
			echo 'Success';
		}else{
			echo 'Unable to Save';
		}
	}

	public function get_edit($id){
		$group = $this->groupModel->where(array("id"=>$id))->findAll();
		if(sizeof($group) > 0)
		{
			$data['group'] = $group[0];
			$data['permissions'] = get_permissions();
			$data['group_permissions'] = $this->groupModel->get_group_permissions($group[0]->id);
			echo view('usergroups/edit',$data);
		}
		else
		{
			echo 'Invalid Group';
		}
	}

	public function Delete($id)
	{
		$this->groupModel->delete($id);
		echo 'Success';
	}

}
