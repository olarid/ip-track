<?php namespace App\Controllers;

use CodeIgniter\Controller;

class Users extends BaseController
{
	protected $session;

	function __construct()
	{
		$this->userModel = model('App\Models\UserModel');
		$this->groupModel = model('App\Models\UserGroupModel');
	}

    public function index(){
		$this->check_access('users', 'view');
		$data['groups'] = $this->groupModel->get_options('');
        echo view('layout/header');
		echo view('users/index', $data);
		echo view('layout/footer');
	}

	public function get_users_req(){
		$users = $this->userModel->where('is_admin', 0)->findAll();
        echo json_encode(array("aaData"=>$users));
	}

	public function save(){

		$data = $this->request->getPost();


		//Check for duplicate
		if($this->userModel->email_exists($data)){
			return 'Email already exist, please use another one';
		}


		//Upload file if it exists
		$pic = isset($data->profile_pic) ? $data->profile_pic : NULL;

		$avatar = $this->request->getFile('image_upload');
		if(isset($avatar)){
			$pic = $avatar->getName();
			$avatar->move(ROOTPATH.'public/assets/uploads/profile-pictures');
		}


		$data['profile_pic'] = $pic;
		$res = $this->userModel->save($data);
		if($res){
			echo "Success";
		}
		else{
			echo $res;
		}
	}

	public function get_edit($id){
		$user = $this->userModel->where(array("id" => $id))->findAll();
		if( sizeof($user) > 0 )
		{
			$user = $user[0];
			$data['user'] = $user;
			$data['groups'] = $this->groupModel->get_options($user->user_group);
			echo view('users/edit',$data);
		}
		else{
			echo "Invalid User";
		}
	}

	public function delete($id){
		$this->userModel->delete($id);
		echo "Success";
	}

}
