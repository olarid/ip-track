<?php namespace App\Controllers;

class Vendors extends BaseController
{
	public function __construct() {
		$this->vendorModel = model('App\Models\VendorModel');
		$this->maskModel = model('App\Models\SubnetMaskModel');
		$this->locationModel = model('App\Models\LocationModel');
		$this->regionModel = model('App\Models\RegionModel');
	}
	public function index()
	{
		$this->check_access('vendors', 'view');
		echo view('layout/header');
		$data['masks'] = $this->maskModel->get_options('');
		$data['locations'] = $this->locationModel->get_options('');
		$data['regions'] = $this->regionModel->get_options('');
		echo view('vendors/index', $data);
		echo view('layout/footer');
	}

	public function get_vendors_req(){
		$records = $this->vendorModel->findAll();
        echo json_encode(array("aaData"=>$records));
	}


	public function save(){
		$data = $this->request->getPost();

		//Check for duplicate
		if($this->vendorModel->email_exists($data)){
			return 'Email already exist, please use another one';
		}

		$res = $this->vendorModel->save($data);
		if($res){
			echo 'Success';
		}else{
			echo 'Unable to Save';
		}
	}

	public function get_edit($id){
		$vendor = $this->vendorModel->where(array("id"=>$id))->findAll();
		if(sizeof($vendor) > 0)
		{
			$data['vendor'] = $vendor[0];
			$data['masks'] = $this->maskModel->get_options($vendor[0]->mask);
			$data['locations'] = $this->locationModel->get_options($vendor[0]->location);
			$data['regions'] = $this->regionModel->get_options($vendor[0]->rir);
			echo view('vendors/edit',$data);
		}
		else
		{
			echo 'Invalid Vendors';
		}
	}

	public function Delete($id)
	{
		$this->vendorModel->delete($id);
		echo 'Success';
	}

}
