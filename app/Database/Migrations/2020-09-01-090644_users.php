<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Users extends Migration
{
	public function up()
	{
		$fields = [
			'id' => [
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => true,
				'unsigned' => true
			],

			'name' => [
				'type' => 'VARCHAR',
				'constraint' => 200,
				'null' => false
			],
			'email' => [
				'type' => 'VARCHAR',
				'constraint' => 200,
				'null' => false,
				'unique' => true
			],
			'password' => [
				'type' => 'VARCHAR',
				'constraint' => 50,
				'null' => false
			],
			'is_admin' => [
				'type' => 'TINYINT',
				'constraint' => 1,
				'default' => 0
			],

			'user_group' => [
				'type' => 'INT',
				'constraint' => 10,
			],
			'group_name' => [
				'type' => 'varchar',
				'constraint' => 100,
			],

			'profile_pic' => [
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => true
			],
			'created_at DATETIME DEFAULT CURRENT_TIMESTAMP',
			'updated_at DATETIME DEFAULT CURRENT_TIMESTAMP'
		];

		$this->forge->addField($fields);
		$this->forge->addPrimaryKey('id');
		$this->forge->createTable('users');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('users');
	}
}
