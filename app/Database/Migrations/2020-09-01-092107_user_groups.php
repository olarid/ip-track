<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class UserGroups extends Migration
{
	public function up()
	{
		$fields = [
			'id' => [
				'type' => 'INT',
				'contraint' => 11,
				'auto_increment' => true,
				'unsigned' => true
			],

			'title' => [
				'type' => 'VARCHAR',
				'constraint' => 20,
				'null' => false
			],
			'created_at DATETIME DEFAULT CURRENT_TIMESTAMP',
			'updated_at DATETIME DEFAULT CURRENT_TIMESTAMP'
		];

		$this->forge->addField($fields);
		$this->forge->addPrimaryKey('id');
		$this->forge->createTable('user_groups');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('user_groups');
	}
}
