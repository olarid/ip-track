<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class SubnetMask extends Migration
{
	public function up()
	{
		$fields = [
			'id' => [
				'type' => 'INT',
				'constraint' => 10,
				'unsigned' => true,
				'auto_increment' => true
			],
			'mask' => [
				'type' => 'INT',
				'constraint' => 10
			],
			'netmask' => [
				'type' => 'varchar',
				'constraint' => 50
			],
			'addresses' => [
				'type' => 'INT',
				'constraint' => 10
			],
			'usable' => [
				'type' => 'INT',
				'constraint' => 10
			],
			'created_at DATETIME DEFAULT CURRENT_TIMESTAMP',
			'updated_at DATETIME DEFAULT CURRENT_TIMESTAMP'
		];

		$this->forge->addField($fields);
		$this->forge->addPrimaryKey('id');
		$this->forge->createTable('subnet_masks');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('subnet_masks');
	}
}
