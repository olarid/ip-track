<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Regions extends Migration
{
	public function up()
	{
		$fields = [
			'id' => [
				'type' => 'INT',
				'constraint' => 10,
				'unsigned' => true,
				'auto_increment' => true
			],
			'region' => [
				'type' => 'varchar',
				'constraint' => 50
			],
			'created_at DATETIME DEFAULT CURRENT_TIMESTAMP',
			'updated_at DATETIME DEFAULT CURRENT_TIMESTAMP'
		];

		$this->forge->addField($fields);
		$this->forge->addPrimaryKey('id');
		$this->forge->createTable('regions');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('regions');
	}
}
