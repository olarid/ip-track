<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Vendors extends Migration
{
	public function up()
	{
		$fields = [
			'id' => [
				'type' => 'INT',
				'constraint' => 10,
				'unsigned' => true,
				'auto_increment' => true
			],
			'name' => [
				'type' => 'varchar',
				'constraint' => 200
			],
			'email' => [
				'type' => 'varchar',
				'constraint' => 100
			],
			'ip' => [
				'type' => 'varchar',
				'constraint' => 50
			],
			'mask' => [
				'type' => 'int',
				'constraint' => 10
			],
			'wholesale_price decimal(18,2) default 0',
			'location' => [
				'type' => 'varchar',
				'constraint' => 100
			],
			'rir' => [
				'type' => 'varchar',
				'constraint' => 100
			],
			'is_assigned' => [
				'type' => 'TINYINT',
				'constraint' => 1,
				'default' => 0
			],
			'created_at DATETIME DEFAULT CURRENT_TIMESTAMP',
			'updated_at DATETIME DEFAULT CURRENT_TIMESTAMP'
		];

		$this->forge->addField($fields);
		$this->forge->addPrimaryKey('id');
		$this->forge->createTable('vendors');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('vendors');
	}
}
