<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Clients extends Migration
{
	public function up()
	{
		$fields = [
			'id' => [
				'type' => 'INT',
				'constraint' => 10,
				'unsigned' => true,
				'auto_increment' => true
			],
			'vendor_id' => [
				'type' => 'INT',
				'constraint' => 10,
			],
			'name' => [
				'type' => 'varchar',
				'constraint' => 200
			],
			'email' => [
				'type' => 'varchar',
				'constraint' => 100
			],
			'main_server' => [
				'type' => 'varchar',
				'constraint' => 50
			],
			'ip' => [
				'type' => 'varchar',
				'constraint' => 50
			],
			'mask' => [
				'type' => 'INT',
				'constraint' => 10
			],
			'delivery_date' => [
				'type' => 'datetime'
			],
			'price_per_ip decimal(18,2) default 0',
			'price_per_allocation decimal(18,2) default 0',
			'term_commitment' => [
				'type' => 'varchar',
				'constraint' => 500
			],
			'created_at DATETIME DEFAULT CURRENT_TIMESTAMP',
			'updated_at DATETIME DEFAULT CURRENT_TIMESTAMP'
		];

		$this->forge->addField($fields);
		$this->forge->addPrimaryKey('id');
		$this->forge->createTable('clients');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('clients');
	}
}
