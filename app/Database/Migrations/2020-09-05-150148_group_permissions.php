<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class GroupPermissions extends Migration
{
	public function up()
	{
		$fields = [
			'id' => [
				'type' => 'INT',
				'constraint' => 10,
				'unsigned' => true,
				'auto_increment' => true
			],
			'group_id' => [
				'type' => 'INT',
				'constraint' => 10
			],
			'module' => [
				'type' => 'varchar',
				'constraint' => 50
			],
			'action' => [
				'type' => 'varchar',
				'constraint' => 20
			],
			'created_at DATETIME DEFAULT CURRENT_TIMESTAMP',
			'updated_at DATETIME DEFAULT CURRENT_TIMESTAMP'
		];

		$this->forge->addField($fields);
		$this->forge->addPrimaryKey('id');
		$this->forge->createTable('group_permissions');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('group_permissions');
	}
}
