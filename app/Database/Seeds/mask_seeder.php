<?php namespace App\Database\Seeds;

class mask_seeder extends \CodeIgniter\Database\Seeder
{
        public function run()
        {
                $masks = array(
                    ['mask'=>32, 'addresses' => 1, 'usable'=>0],
                    ['mask'=>31, 'addresses' => 2, 'usable'=>0],
                    ['mask'=>30, 'addresses' => 4, 'usable'=>2],
                    ['mask'=>29, 'addresses' => 8, 'usable'=>6],
                    ['mask'=>28, 'addresses' => 16, 'usable'=>14],
                    ['mask'=>27, 'addresses' => 32, 'usable'=>30],
                    ['mask'=>26, 'addresses' => 64, 'usable'=>62],
                    ['mask'=>25, 'addresses' => 128, 'usable'=>126],
                    ['mask'=>24, 'addresses' => 256, 'usable'=>254],
                    ['mask'=>23, 'addresses' => 512, 'usable'=>510],
                    ['mask'=>22, 'addresses' => 1024, 'usable'=>1022],
                    ['mask'=>21, 'addresses' => 2048, 'usable'=>2046],
                    ['mask'=>20, 'addresses' => 4096, 'usable'=>4094],
                    ['mask'=>19, 'addresses' => 8192, 'usable'=>8190],
                    ['mask'=>18, 'addresses' => 16384, 'usable'=>16382],
                    ['mask'=>17, 'addresses' => 32768, 'usable'=>32766],
                    ['mask'=>16, 'addresses' => 65536, 'usable'=>65534],
                    ['mask'=>15, 'addresses' => 131072, 'usable'=>131070],
                    ['mask'=>14, 'addresses' => 262144, 'usable'=>262142],
                    ['mask'=>13, 'addresses' => 524288, 'usable'=>524286],
                    ['mask'=>12, 'addresses' => 1048576, 'usable'=>1048574],
                    ['mask'=>11, 'addresses' => 2097152, 'usable'=>2097150],
                    ['mask'=>10, 'addresses' => 4194304, 'usable'=>4194302],
                    ['mask'=>9, 'addresses' => 8388608, 'usable'=>8388606],
                    ['mask'=>8, 'addresses' => 16777216, 'usable'=>16777214]
                );

                foreach($masks as $m){
                    $this->db->table('subnet_masks')->insert($m);
                }
        }
}