<?php namespace App\Database\Seeds;

class region_seeder extends \CodeIgniter\Database\Seeder
{
        public function run()
        {
                $data = array(
                    ['region'=>'Ripe'],
                    ['region'=>'Lacnic'],
                    ['region'=>'Afrinic'],
                    ['region'=>'Arin'],
                    ['region'=>'Apnic']
                );

                foreach($data as $d){
                    $this->db->table('regions')->insert($d);
                }
        }
}