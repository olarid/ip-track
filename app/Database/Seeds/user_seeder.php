<?php namespace App\Database\Seeds;

class user_seeder extends \CodeIgniter\Database\Seeder
{
        public function run()
        {
                $data = array(
                    ['name'=>'Admin User', 'email' => 'admin@gmail.com', 'password' => 'adm$_1892', 'is_admin' => 1]
                );

                foreach($data as $d){
                    $this->db->table('users')->insert($d);
                }
        }
}