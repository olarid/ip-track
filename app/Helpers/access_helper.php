<?php
    function get_menus(){

        //Main Menus
        $main = array();
        $main[] = [
            'title' => 'Dashboard',
            'position' => 1,
            'url' => base_url().'/home',
            'icon' => 'zmdi zmdi-view-dashboard',
            'type' => 'main'
        ];

        if(has_permission('clients', 'view')){
            $main[] = [
                'title' => 'Clients',
                'position' => 5,
                'url' => base_url().'/clients',
                'icon' => 'zmdi zmdi-card-travel',
                'type' => 'main'
            ];
        }

        if(has_permission('vendors', 'view')){
            $main[] = [
                'title' => 'Vendors',
                'position' => 10,
                'url' => base_url().'/vendors',
                'icon' => 'zmdi zmdi-store',
                'type' => 'main'
            ];
        }

        if(has_permission('users', 'view')){
            $main[] = [
                'title' => 'Users',
                'position' => 15,
                'url' => base_url().'/users',
                'icon' => 'zmdi zmdi-accounts',
                'type' => 'main'
            ];
        }

        //Setup Menus
        $setup = array();
        if(has_permission('regions', 'view')){
            $setup[] = [
                'title' => 'Regions',
                'position' => 20,
                'url' => base_url().'/regions',
                'icon' => 'zmdi zmdi-city-alt',
                'type' => 'setup'
            ];
        }

        if(has_permission('locations', 'view')){
            $setup[] = [
                'title' => 'Locations',
                'position' => 25,
                'url' => base_url().'/locations',
                'icon' => 'zmdi zmdi-globe',
                'type' => 'setup'
            ];
        }
        if(has_permission('subnetmasks', 'view')){
            $setup[] = [
                'title' => 'Subnet Masks',
                'position' => 30,
                'url' => base_url().'/SubnetMasks',
                'icon' => 'zmdi zmdi-bug',
                'type' => 'setup'
            ];
        }
        if(has_permission('usergroups', 'view')){
            $setup[] = [
                'title' => 'User Groups',
                'position' => 35,
                'url' => base_url().'/UserGroups',
                'icon' => 'zmdi zmdi-accounts-list',
                'type' => 'setup'
            ];
        }

        return array('Main' => $main, 'Setups' => $setup);
    }

    function has_permission($module, $action){
        $user = session('ss_user');
        if($user->is_admin == true){
            return true;
        }
        $groupModel = model('App\Models\UserGroupModel');
        return $groupModel->check_permission($user->user_group, $module, $action);
    }

    function get_permissions(){
        $perm = array(
            array('title'=>'Dashboard','slug'=>'dashboard','actions' => ['profile']),
            array('title'=>'Clients','slug'=>'clients','actions' => ['view', 'add', 'edit', 'delete']),
            array('title'=>'Vendors','slug'=>'vendors','actions' => ['view', 'add', 'edit', 'delete']),
            array('title'=>'Users','slug'=>'users','actions' => ['view', 'add', 'edit', 'delete']),
            array('title'=>'Regions','slug'=>'regions','actions' => ['view', 'add', 'edit', 'delete']),
            array('title'=>'Locations','slug'=>'locations','actions' => ['view', 'add', 'edit', 'delete']),
            array('title'=>'Subnet Masks','slug'=>'subnetmasks','actions' => ['view', 'add', 'edit', 'delete']),
            array('title'=>'User Groups','slug'=>'usergroups','actions' => ['view', 'add', 'edit', 'delete'])
        );

        return $perm;
    }