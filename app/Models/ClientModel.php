<?php namespace App\Models;

use CodeIgniter\Model;

class ClientModel extends Model
{
    protected $table = 'clients';
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'vendor_id','name','email','main_server','ip','mask','delivery_date','price_per_ip','price_per_allocation', 'term_commitment'
    ];
    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $returnType     = 'object';

    public function email_exists($data){
        if(isset($data['id'])){
            $cmdtext = "select * from clients where email = '".$data['email']."' and id <> ".$data['id'];
        }else{
            $cmdtext = "select * from clients where email = '".$data['email']."'";
        }

        $query = $this->db->query($cmdtext);
        if(sizeof($query->getResult()) > 0){
            return true;
        }
        return false;
    }
}