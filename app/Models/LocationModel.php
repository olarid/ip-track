<?php namespace App\Models;

use CodeIgniter\Model;

class LocationModel extends Model
{
    protected $table = 'locations';
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'location'
    ];
    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $returnType     = 'object';


    public function get_options($value){
        $records = $this->findAll();
        $options = '';
        foreach($records as $r){
            $options .= ('<option value="'.$r->location.'" '.($r->location == $value?'selected':'').'>'.$r->location.'</option>');
        }
        return $options;
    }
}