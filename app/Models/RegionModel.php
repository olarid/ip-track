<?php namespace App\Models;

use CodeIgniter\Model;

class RegionModel extends Model
{
    protected $table = 'regions';
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'region'
    ];
    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $returnType     = 'object';

    public function get_options($value){
        $records = $this->findAll();
        $options = '';
        foreach($records as $r){
            $options .= ('<option value="'.$r->region.'" '.($r->region == $value?'selected':'').'>'.$r->region.'</option>');
        }
        return $options;
    }
}