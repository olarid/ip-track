<?php namespace App\Models;

use CodeIgniter\Model;

class SubnetMaskModel extends Model
{
    protected $table = 'subnet_masks';
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'mask', 'addresses', 'usable'
    ];
    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $returnType     = 'object';

    public function get_options($value){
        $records = $this->findAll();
        $options = '';
        foreach($records as $r){
            $options .= ('<option value="'.$r->mask.'" '.($r->mask == $value?'selected':'').'>/'.$r->mask.' ('.$r->addresses.' ips)'.'</option>');
        }
        return $options;
    }
}