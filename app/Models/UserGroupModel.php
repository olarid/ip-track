<?php namespace App\Models;

use CodeIgniter\Model;

class UserGroupModel extends Model
{
    protected $table = 'user_groups';
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'title'
    ];
    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $returnType     = 'object';

    public function get_options($value){
        $records = $this->findAll();
        $options = '';
        foreach($records as $r){
            $options .= ('<option value="'.$r->id.'" '.($r->id == $value?'selected':'').'  data-name="'.
            $r->title.'" >'.$r->title.'</option>');
        }
        return $options;
    }

    public function max_id(){
        return $this->db->query('select max(id) maxid from user_groups')->getRow()->maxid;
    }

    public function add_permissions($groupid, $permissions){
        $cmdtext = "Insert into group_permissions (group_id, module, action) ";
        $cnt = 0;
        foreach ($permissions as $key => $value) {
            $pieces = explode("_", $key);
            $module = $pieces[0];
            $action = $pieces[1];
            if($cnt == 0){
                $cmdtext .= (" select ".$groupid.", '".$module."', '".$action."' ");
            }else{
                $cmdtext .= (" union select ".$groupid.", '".$module."', '".$action."' ");
            }
            $cnt++;
        }

        $this->db->query($cmdtext);
    }

    public function purge_permissions($groupid){
        $cmdtext = "delete from group_permissions where group_id = ".$groupid;
        $this->db->query($cmdtext);
    }

    public function get_group_permissions($groupid){
        $cmdtext = "select * from group_permissions where group_id = ".$groupid;
        $query =  $this->db->query($cmdtext);
        return $query->getResult();
    }

    public function check_permission($groupid, $module, $action){
        $cmdtext = "select * from group_permissions where group_id = ".$groupid."  and module = '".$module."' and action = '".$action."'";
        $query = $this->db->query($cmdtext);
        if(sizeof($query->getResult()) > 0){
            return true;
        }
        return false;
    }
}