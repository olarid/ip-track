<?php namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model
{
    protected $table = 'users';
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'name', 'email', 'password', 'user_group', 'group_name', 'profile_pic'
    ];
    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $returnType     = 'object';

    public function email_exists($data){
        if(isset($data['id'])){
            $cmdtext = "select * from users where email = '".$data['email']."' and id <> ".$data['id'];
        }else{
            $cmdtext = "select * from users where email = '".$data['email']."'";
        }

        $query = $this->db->query($cmdtext);
        if(sizeof($query->getResult()) > 0){
            return true;
        }
        return false;
    }
}