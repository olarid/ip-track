<?php namespace App\Models;

use CodeIgniter\Model;

class VendorModel extends Model
{
    protected $table = 'vendors';
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'name','email','ip','mask','wholesale_price','location','rir', 'is_assigned'
    ];
    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $returnType     = 'object';


    public function get_options($value){
        $records = $this->where(array('is_assigned' => false))->findAll();
        $options = '';
        foreach($records as $r){
            $options .= ('<option value="'.$r->ip.'" '.($r->id == $value?'selected':'').'  data-id = "'.$r->id.'" data-mask="'.$r->mask.'" >'.
                        $r->ip.'/'.$r->mask.' ('.$r->rir.')'.'</option>');
        }
        return $options;
    }

    public function email_exists($data){
        if(isset($data['id'])){
            $cmdtext = "select * from vendors where email = '".$data['email']."' and id <> ".$data['id'];
        }else{
            $cmdtext = "select * from vendors where email = '".$data['email']."'";
        }

        $query = $this->db->query($cmdtext);
        if(sizeof($query->getResult()) > 0){
            return true;
        }
        return false;
    }
    public function ip_assigned($id){
        $vendor = $this->where(array("id"=>$id, 'is_assigned' => 1))->findAll();
        if(sizeof($vendor) > 0){
            return true;
        }
        return false;
    }
}