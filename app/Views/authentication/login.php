<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Login | IP Track</title>
    <!--favicon-->
    <link rel="icon" href="<?php echo base_url() ?>/assets/images/logo-icon.png" type="image/x-icon">
    <!-- Bootstrap core CSS-->
    <link href="<?php echo base_url() ?>/assets/css/bootstrap.min.css" rel="stylesheet" />
    <!-- animate CSS-->
    <link href="<?php echo base_url() ?>/assets/css/animate.css" rel="stylesheet" type="text/css" />
    <!-- Icons CSS-->
    <link href="<?php echo base_url() ?>/assets/css/icons.css" rel="stylesheet" type="text/css" />
    <!-- Custom Style-->
    <link href="<?php echo base_url() ?>/assets/css/app-style.css" rel="stylesheet" />


    <!-- notifications css -->
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/plugins/notifications/css/lobibox.min.css"/>


    <script>
        window.baseUrl = '<?php echo base_url() ?>/';
    </script>

</head>

<body class="bg-theme bg-theme6">

    <!-- start loader -->
    <div id="pageloader-overlay" class="visible incoming">
        <div class="loader-wrapper-outer">
            <div class="loader-wrapper-inner">
                <div class="loader"></div>
            </div>
        </div>
    </div>
    <!-- end loader -->

    <!-- Start wrapper-->
    <div id="wrapper">

        <div class="loader-wrapper">
            <div class="lds-ring">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
        <div class="card card-authentication1 mx-auto my-5" style="margin-top: 10rem!important;">
            <div class="card-body">
                <div class="card-content p-2">
                    <div class="text-center">
                        <img src="<?php echo base_url() ?>/assets/images/logo-icon.png" alt="logo icon">
                    </div>
                    <div class="card-title text-uppercase text-center py-3">Sign In</div>
                    <form  method="post" action="<?php echo base_url(); ?>/home/login" id="frmLogin" >
                        <div class="form-group">
                            <label for="email" class="sr-only">Email</label>
                            <div class="position-relative has-icon-right">
                                <input type="email" id="email" name="email" class="form-control input-shadow" placeholder="Enter Email">
                                <div class="form-control-position">
                                    <i class="icon-user"></i>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="sr-only">Password</label>
                            <div class="position-relative has-icon-right">
                                <input type="password" id="password" name="password" class="form-control input-shadow" placeholder="Enter Password">
                                <div class="form-control-position">
                                    <i class="icon-lock"></i>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="text-danger" style="text-transform: none;"><?php echo $message ?></label>
                        </div>
                        <button type="submit" class="btn btn-light btn-block">Sign In</button>

                    </form>
                </div>
            </div>
        </div>

        <!--Start Back To Top Button-->
        <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
        <!--End Back To Top Button-->



    </div>
    <!--wrapper-->

    <!-- Bootstrap core JavaScript-->
    <script src="<?php echo base_url() ?>/assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/popper.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/bootstrap.min.js"></script>

    <!-- sidebar-menu js -->
    <script src="<?php echo base_url() ?>/assets/js/sidebar-menu.js"></script>

    <!-- Custom scripts -->
    <script src="<?php echo base_url() ?>/assets/js/app-script.js"></script>


    <!--Form Validatin Script-->
    <script src="<?php echo base_url() ?>/assets/plugins/jquery-validation/js/jquery.validate.min.js"></script>

    <!-- notification js  -->
    <script src="<?php echo base_url() ?>/assets/plugins/notifications/js/lobibox.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/plugins/notifications/js/notifications.min.js"></script>

    <script>
        $(document).ready(function(){
            $("#frmLogin").validate({
                rules: {
                    email: "required",
                    password: "required"
                }
            });

        })
    </script>

</body>

</html>