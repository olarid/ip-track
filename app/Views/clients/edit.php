<div class="modal-dialog" style="max-width: 600px;">
    <div class="modal-content animated fadeInUp">
        <div class="modal-header">
        <h5 class="modal-title">Edit Client</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
                <form  method="post" action="<?php echo base_url(); ?>Clients/Edit" id="frmEdit">
                    <input type="hidden" name="edt_id" id="edt_id" value="<?php echo $client->id ?>" />
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Full Name</label>
                                <input class="form-control" type="text" id="edt_name" name="edt_name" value="<?php echo $client->name ?>" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Email</label>
                                <input class="form-control" type="email" id="edt_email" name="edt_email" value="<?php echo $client->email ?>" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Main Server IP (e.g. xxx.xxx.xxx.xxx)</label>
                                <input class="form-control" type="text" id="edt_main_server" name="edt_main_server" value="<?php echo $client->main_server ?>"  placeholder = " (e.g. xxx.xxx.xxx.xxx)"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">IP Allocation</label>
                                <select class="selectpicker form-control"  data-live-search="true"  name="edt_ip" id="edt_ip"><?php echo $vendors ?></select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Delivery Date</label>
                                <input type="date" class="form-control" id="edt_delivery_date" name="edt_delivery_date" value="<?php echo $client->delivery_date ?>"  />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Retail Price Paid by IP</label>
                                <input type="number" class="form-control" name="edt_price_per_ip" id = "edt_price_per_ip" value="<?php echo $client->price_per_ip ?>"  />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Price Paid Per Allocation</label>
                                <input type="number" class="form-control" name="edt_price_per_allocation" id = "edt_price_per_allocation" value="<?php echo $client->price_per_allocation ?>"  />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Term Commitment</label>
                                <input class="form-control" type="text" id="edt_term_commitment" name="edt_term_commitment" value="<?php echo $client->term_commitment ?>" />
                            </div>
                        </div>
                    </div>

                </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            <button type="button" class="btn btn-light" onclick="editSave()"><i class="fa fa-check-square-o"></i> Save changes</button>
        </div>
    </div>
</div>

<script>

    $(document).ready(function(){
         // Input validator
        $("#frmEdit").validate({
                rules: {
                    name: "required",
                    delivery_date: "required",
                    ip:  {
                        required: true,
                        IP4Checker: true
                    },
                    main_server:  {
                        required: true,
                        IP4Checker: true
                    }
                }
        });

        $('.selectpicker').selectpicker();
    });

    function editSave(){
        if(!$("#frmEdit").valid()){
            return;
        }

        //Serialize form data
        var frmData = {};
        var info = $('#frmEdit').serializeArray({checkboxesAsBools: false});

        //Remove edt_ from fields
        $.each(info, function (key, input) {
            frmData[input.name.replace('edt_', '')] = input.value;
        });

        //get mask and vendor id
        frmData['vendor_id'] = $('#edt_ip :selected').data('id');
        frmData['mask'] = $('#edt_ip :selected').data('mask');

        //Send data to server
        $("#loaderwrapper").show();
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url() ?>/clients/save',
            data: frmData,
            success: function (response) {
                if (response == "Success") {
                    $("#loaderwrapper").hide();
                    swal("Success", 'Record updated successfully', 'success');
                    tbData.ajax.reload();
                    $('#mdEdit').modal('hide');
                }
                else {
                    $("#loaderwrapper").hide();
                    swal("Status",response,"info");
                }
            },
            error: function () {
                $("#loaderwrapper").hide();
                swal("Error", 'Unable to update recored, please try again', "error");
            }
        });

    }

</script>