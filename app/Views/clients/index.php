<!-- Start BreadCrumb -->
<div class="row pt-2 pb-2">
    <div class="col-sm-12">
        <h4 class="page-title">Clients</h4>
    </div>
</div>
<!-- End BreadCrumb -->

<!-- Begin Table -->
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <strong class=" text-uppercase">Manage Clients</strong>
                <?php if(has_permission('clients', 'add') ) {?>
                    <button class="btn btn-light card-btn" onclick="$('#mdAdd').modal('show');">Add</button>
                <?php } ?>
            </div>
            <div class="card-body">
                <div class="table-responsive">

                    <table id="tbData" class="table table-bordered" style="width: 100%">
                        <thead>
                            <tr>
                                <th>Client Name</th>
                                <th>Email</th>
                                <th>Main Server</th>
                                <th>IP Address</th>
                                <th>Delivery Date</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<!--End Table-->

<!-- Add Client Modal -->
<div class="modal fade" id="mdAdd">
    <div class="modal-dialog" style="max-width: 600px;">
        <div class="modal-content animated fadeInUp">
            <div class="modal-header">
            <h5 class="modal-title">Add Client</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                    <form  method="post" action="<?php echo base_url(); ?>Clients/Add" id="frmAdd">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Full Name</label>
                                    <input class="form-control" type="text" id="name" name="name"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Email</label>
                                    <input class="form-control" type="email" id="email" name="email"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Main Server IP (e.g. xxx.xxx.xxx.xxx)</label>
                                    <input class="form-control" type="text" id="main_server" name="main_server" placeholder = " (e.g. xxx.xxx.xxx.xxx)"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">IP Allocation</label>
                                    <select class="selectpicker form-control"  data-live-search="true"  name="ip" id="ip"><?php echo $vendors ?></select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Delivery Date</label>
                                    <input type="date" class="form-control" id="delivery_date" name="delivery_date" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Retail Price Paid by IP</label>
                                    <input type="number" class="form-control" name="price_per_ip" id = "price_per_ip" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Price Paid Per Allocation</label>
                                    <input type="number" class="form-control" name="price_per_allocation" id = "price_per_allocation" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Term Commitment</label>
                                    <input class="form-control" type="text" id="term_commitment" name="term_commitment"/>
                                </div>
                            </div>
                        </div>

                    </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="button" class="btn btn-light" onclick="addSave()"><i class="fa fa-check-square-o"></i> Save </button>
            </div>
        </div>
    </div>
</div>
<!-- End Add Modal  -->

<div class="modal fade" id="mdEdit"></div>

<script>
    var tbData;
    $(document).ready(function(){
        //Validator for add form
        $("#frmAdd").validate({
                rules: {
                    name: "required",
                    delivery_date: "required",
                    ip:  {
                        required: true,
                        IP4Checker: true
                    },
                    main_server:  {
                        required: true,
                        IP4Checker: true
                    }
                }
        });

        var lurl = "<?php echo base_url(); ?>/clients/get_clients_req";
        tbData = $('#tbData').DataTable({
            lengthChange: true,
            "bRetrieve": true,
            "sAjaxSource": lurl,
            "aoColumns": [
                { data: 'name' },
                { data: 'email' },
                { data: 'main_server' },
                {
                    data: 'ip',
                    render: function(data, type, row){
                        return data + '/' + row['mask'];
                    }
                },
                {
                    data: 'delivery_date',
                    render: function(data){
                        return moment(data).format('MM/DD/YYYY');
                    }
                },
                {
                    data: 'id',
                    render: function(data){
                        var btn = '';
                        <?php if(has_permission('clients', 'edit') ) {?>
                            btn += '<span class="tbAction"  onclick="Edit('+data+')"><i class="ti-marker-alt"></i></span>';
                        <?php } ?>
                        <?php if(has_permission('clients', 'delete') ) {?>
                            btn += '<span class="tbAction" onclick="Delete('+data+')"><i class="ti-trash"></i></span>';
                        <?php } ?>
                        return btn;
                    }
                }
            ]

        });
      });

    function addSave(){
        if(!$("#frmAdd").valid()){
            return;
        }

        //Serialize form data
        var frmData = {};
        var info = $('#frmAdd').serializeArray({checkboxesAsBools: false});
        $.each(info, function (key, input) {
            frmData[input.name] = input.value;
        });

        //get mask and vendor id
        frmData['vendor_id'] = $('#ip :selected').data('id');
        frmData['mask'] = $('#ip :selected').data('mask');

        //Send data to server
        $("#loaderwrapper").show();
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url() ?>/clients/save',
            data: frmData,
            success: function (response) {
                if (response == "Success") {
                    $("#loaderwrapper").hide();
                    swal("Success", 'Record added successfully', 'success');
                    tbData.ajax.reload();
                    $('#mdAdd').modal('hide');
                }
                else {
                    $("#loaderwrapper").hide();
                    swal("Status",response,"info");
                }
            },
            error: function () {
                $("#loaderwrapper").hide();
                swal("Error", 'Unable to add recored, please try again', "error");
            }
        });

    }

    function Edit(id)
    {
        $("#loaderwrapper").show();
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url(); ?>/clients/get_edit/'+id,
            data: {},
            success: function (response) {
                $("#loaderwrapper").hide();

                $('#mdEdit').html(response);
                $('#mdEdit').modal('show');

            },
            error: function (response) {

                $("#loaderwrapper").hide();
                swal("Error", response, "error");
            }
        });
    }

      function Delete(id)
      {
          if(!confirm('Are you sure you want to delete this record, this action is not reversible?'))
          {
                return;
          }

          $("#loaderwrapper").show();
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url(); ?>/clients/delete/'+id,
                data: {},
                success: function (response) {
                    if (response == "Success") {
                        $("#loaderwrapper").hide();
                        swal("Success", 'Record deleted successfully', 'success');
                        tbData.ajax.reload();
                    }
                    else {
                        $("#loaderwrapper").hide();
                        swal("Status",response,"info");
                    }
                },
                error: function (response) {

                    $("#loaderwrapper").hide();
                    swal("Error", response, "error");
                }
            });


      }
</script>
