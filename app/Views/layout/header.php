<?php
    $menus =  session('menus');
    $user =  session('ss_user');
    helper('access_helper');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>IP Track | Managing IP Resources</title>
    <!--favicon-->
    <link rel="icon" href="<?php echo base_url() ?>/assets/images/logo-icon.png" type="image/x-icon">
    <!-- Vector CSS -->
    <link href="<?php echo base_url() ?>/assets/plugins/vectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet" />
    <!-- simplebar CSS-->
    <link href="<?php echo base_url() ?>/assets/plugins/simplebar/css/simplebar.css" rel="stylesheet" />
    <!-- Bootstrap core CSS-->
    <link href="<?php echo base_url() ?>/assets/css/bootstrap.min.css" rel="stylesheet" />
    <!-- animate CSS-->
    <link href="<?php echo base_url() ?>/assets/css/animate.css" rel="stylesheet" type="text/css" />
    <!-- Icons CSS-->
    <link href="<?php echo base_url() ?>/assets/css/icons.css" rel="stylesheet" type="text/css" />
    <!-- Sidebar CSS-->
    <link href="<?php echo base_url() ?>/assets/css/sidebar-menu.css" rel="stylesheet" />
    <!-- Custom Style-->
    <link href="<?php echo base_url() ?>/assets/css/app-style.css" rel="stylesheet" />


    <!--Data Tables -->
    <link href="<?php echo base_url() ?>/assets/plugins/bootstrap-datatable/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>/assets/plugins/bootstrap-datatable/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css">


    <!-- Bootstrap select -->
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/plugins/bootstrap-select/dist/css/bootstrap-select.min.css">


    <!-- Loader -->
    <link href="<?php echo base_url() ?>/assets/plugins/Loader/Loader.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>/assets/css/custom.css" rel="stylesheet" />

    <style>

        .btSubmit{
            margin-top: 30px;
            border-top: 1px solid;
            padding-top: 20px;
        }

        .tbAction{
            cursor: pointer;
            margin: 10px;
        }

        /* Overrite Modal styling */

        .modal-content {
            background: ##1a262b;
        }

        /* Overwrite swal styling  */
        .swal-modal {
            background-color: ##1a262b;
        }
    </style>






    <!-- Bootstrap core JavaScript-->
    <script src="<?php echo base_url() ?>/assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/popper.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/bootstrap.min.js"></script>

    <!-- simplebar js -->
    <script src="<?php echo base_url() ?>/assets/plugins/simplebar/js/simplebar.js"></script>
    <!-- sidebar-menu js -->
    <script src="<?php echo base_url() ?>/assets/js/sidebar-menu.js"></script>
    <!-- Custom scripts -->
    <script src="<?php echo base_url() ?>/assets/js/app-script.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/customFix.js"></script>
    <!-- Chart js -->

    <script src="<?php echo base_url() ?>/assets/plugins/Chart.js/Chart.min.js"></script>
    <!-- Vector map JavaScript -->
    <script src="<?php echo base_url() ?>/assets/plugins/vectormap/jquery-jvectormap-2.0.2.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/plugins/vectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- Easy Pie Chart JS -->
    <script src="<?php echo base_url() ?>/assets/plugins/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
    <!-- Sparkline JS -->
    <script src="<?php echo base_url() ?>/assets/plugins/sparkline-charts/jquery.sparkline.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/plugins/jquery-knob/excanvas.js"></script>
    <script src="<?php echo base_url() ?>/assets/plugins/jquery-knob/jquery.knob.js"></script>


    <!--Form Validatin Script-->
    <script src="<?php echo base_url() ?>/assets/plugins/jquery-validation/js/jquery.validate.min.js"></script>

    <script>
        window.baseUrl = '<?php echo base_url() ?>/';
    </script>




      <!--Data Tables js-->
    <script src="<?php echo base_url() ?>/assets/plugins/bootstrap-datatable/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/plugins/bootstrap-datatable/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/plugins/bootstrap-datatable/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/plugins/bootstrap-datatable/js/buttons.bootstrap4.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/plugins/bootstrap-datatable/js/jszip.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/plugins/bootstrap-datatable/js/pdfmake.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/plugins/bootstrap-datatable/js/vfs_fonts.js"></script>
    <script src="<?php echo base_url() ?>/assets/plugins/bootstrap-datatable/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/plugins/bootstrap-datatable/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/plugins/bootstrap-datatable/js/buttons.colVis.min.js"></script>

    <!-- Sweet alert -->
    <script src="<?php echo base_url() ?>/assets/plugins/alerts-boxes/js/sweetalert.min.js"></script>

    <!-- Moment -->
    <script src="<?php echo base_url() ?>/assets/plugins/moment/min/moment.min.js"></script>




    <!-- Bootstrap select -->
    <script src="<?php echo base_url() ?>/assets/plugins/bootstrap-select/dist/js/bootstrap-select.min.js"></script>


</head>

<body class="bg-theme bg-theme6">

    <div id="loaderwrapper" class="lds-ring" style="display:none"><div></div><div></div><div></div><div></div></div>

    <!-- start loader -->
    <div id="pageloader-overlay" class="visible incoming">
        <div class="loader-wrapper-outer">
            <div class="loader-wrapper-inner">
                <div class="loader"></div>
            </div>
        </div>
    </div>
    <!-- end loader -->


    <!-- Start wrapper-->
    <div id="wrapper">

        <!--Start sidebar-wrapper-->
        <div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
            <div class="brand-logo">
                <a href="index.html">
                    <img src="<?php echo base_url() ?>/assets/images/logo-icon.png" class="logo-icon"  alt="logo icon">
                    <h5 class="logo-text">IP Track</h5>
                </a>
            </div>

            <!-- https://via.placeholder.com/110x110 -->
            <div class="user-details">
                <div class="media align-items-center user-pointer collapsed" data-toggle="collapse" data-target="#user-dropdown">
                    <div class="avatar"><img class="mr-3 side-user-img" src="https://via.placeholder.com/110x110" alt="user avatar"></div>
                    <div class="media-body">
                        <h6 class="side-user-name"><?php echo $user->name ?></h6>
                    </div>
                </div>
                <div id="user-dropdown" class="collapse">
                    <ul class="user-setting-menu">
                        <?php if(has_permission('dashboard', 'profile') ) {?>
                            <li><a href="<?php echo base_url() ?>/home/updateprofile"><i class="icon-user"></i>  My Profile</a></li>
                        <?php } ?>
                        <li><a href="<?php echo base_url() ?>/home/logout"><i class="icon-power"></i> Logout</a></li>
                    </ul>
                </div>
            </div>
            <ul class="sidebar-menu">
                <li class="sidebar-header">MAIN NAVIGATION</li>
                <?php foreach($menus['Main'] as $m){ ?>
                    <li>
                        <a href="<?php echo $m['url'] ?>" class="waves-effect">
                            <i class="<?php echo $m['icon'] ?>"></i> <span><?php echo $m['title'] ?></span>
                        </a>
                    </li>
                <?php } ?>

                <li class="sidebar-header">SETUPS</li>
                <?php foreach($menus['Setups'] as $m){ ?>
                    <li>
                        <a href="<?php echo $m['url'] ?>" class="waves-effect">
                            <i class="<?php echo $m['icon'] ?>"></i> <span><?php echo $m['title'] ?></span>
                        </a>
                    </li>
                <?php } ?>

            </ul>

        </div>
        <!--End sidebar-wrapper-->

        <!--Start topbar header-->
        <header class="topbar-nav">
            <nav class="navbar navbar-expand fixed-top">
                <ul class="navbar-nav mr-auto align-items-center">
                    <li class="nav-item">
                        <a class="nav-link toggle-menu" href="javascript:void();">
                            <i class="icon-menu menu-icon"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <form class="search-bar">
                            <input type="text" class="form-control" placeholder="Enter keywords">
                            <a href="javascript:void();"><i class="icon-magnifier"></i></a>
                        </form>
                    </li>
                </ul>

                <ul class="navbar-nav align-items-center right-nav-link">

                    <li class="nav-item dropdown-lg">
                        <a class="nav-link dropdown-toggle dropdown-toggle-nocaret waves-effect" data-toggle="dropdown" href="javascript:void();">
                            <i class="fa fa-bell-o"></i><span class="badge badge-info badge-up">5</span></a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    You have 1 Notifications
                                    <span class="badge badge-info">5</span>
                                </li>
                                <li class="list-group-item">
                                    <a href="javaScript:void();">
                                        <div class="media">
                                            <i class="zmdi zmdi-accounts fa-2x mr-3 text-info"></i>
                                            <div class="media-body">
                                                <h6 class="mt-0 msg-title">New Registered Users</h6>
                                                <p class="msg-info">Lorem ipsum dolor sit amet...</p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="list-group-item text-center"><a href="javaScript:void();">See All Notifications</a></li>
                            </ul>
                        </div>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" data-toggle="dropdown" href="#">
                            <span class="user-profile"><img src="https://via.placeholder.com/110x110" class="img-circle" alt="user avatar"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li class="dropdown-item user-details">
                                <a href="javaScript:void();">
                                    <div class="media">
                                        <div class="avatar"><img class="align-self-start mr-3" src="https://via.placeholder.com/110x110" alt="user avatar"></div>
                                        <div class="media-body">
                                            <h6 class="mt-2 user-title"><?php echo $user->name ?></h6>
                                            <p class="user-subtitle"><?php echo$user->email ?></p>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="dropdown-divider"></li>
                            <li class="dropdown-item" style="cursor: pointer" onclick="window.location.href='<?php echo base_url()?>/home/logout'" ><i class="icon-power mr-2"></i> Logout</li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </header>
        <!--End topbar header-->

        <div class="clearfix"></div>

        <div class="content-wrapper">
            <div class="container-fluid">

