<div class="modal-dialog" style="max-width: 400px;">
    <div class="modal-content animated fadeInUp">
        <div class="modal-header">
        <h5 class="modal-title">Edit Subnet Mask</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
                <form  method="post" action="<?php echo base_url(); ?>SubnetMasks/Edit" id="frmEdit">
                    <input type="hidden" name="edt_id" id="edt_id" value="<?php echo $mask->id ?>" />
                    <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Mask (e.g 32)</label>
                                    <input class="form-control" type="number" id="edt_mask" name="edt_mask" value = "<?php echo $mask->mask ?>"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Total IP Addresses</label>
                                    <input class="form-control" type="number" id="edt_addresses" name="edt_addresses"  value = "<?php echo $mask->addresses ?>"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Total Usable IP</label>
                                    <input class="form-control" type="number" id="edt_usable" name="edt_usable"  value = "<?php echo $mask->usable ?>"/>
                                </div>
                            </div>
                        </div>
                </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            <button type="button" class="btn btn-light" onclick="editSave()"><i class="fa fa-check-square-o"></i> Save changes</button>
        </div>
    </div>
</div>

<script>

    $(document).ready(function(){
         // Input validator
        $("#frmEdit").validate({
                rules: {
                    mask: "required",
                    addresses: "required"
                }
        });
    });

    function editSave(){
        if(!$("#frmEdit").valid()){
            return;
        }

        //Serialize form data
        var frmData = {};
        var info = $('#frmEdit').serializeArray({checkboxesAsBools: false});

        //Remove edt_ from fields
        $.each(info, function (key, input) {
            frmData[input.name.replace('edt_', '')] = input.value;
        });

        //Send data to server
        $("#loaderwrapper").show();
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url() ?>/SubnetMasks/save',
            data: frmData,
            success: function (response) {
                if (response == "Success") {
                    $("#loaderwrapper").hide();
                    swal("Success", 'Record updated successfully', 'success');
                    tbData.ajax.reload();
                    $('#mdEdit').modal('hide');
                }
                else {
                    $("#loaderwrapper").hide();
                    swal("Status",response,"info");
                }
            },
            error: function () {
                $("#loaderwrapper").hide();
                swal("Error", 'Unable to update recored, please try again', "error");
            }
        });

    }

</script>