<!-- Start BreadCrumb -->
<div class="row pt-2 pb-2">
    <div class="col-sm-12">
        <h4 class="page-title">Subnet Masks</h4>
    </div>
</div>
<!-- End BreadCrumb -->

<!-- Begin Table -->
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <strong class=" text-uppercase">Manage Subnet Masks</strong>
                <?php if(has_permission('subnetmasks', 'add') ) {?>
                    <button class="btn btn-light card-btn" onclick="$('#mdAdd').modal('show');">Add</button>
                <?php } ?>
            </div>
            <div class="card-body">
                <div class="table-responsive">

                    <table id="tbData" class="table table-bordered" style="width: 100%">
                        <thead>
                            <tr>
                                <th>Masks</th>
                                <th># of Addresses</th>
                                <th># of Usable</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<!--End Table-->

<!-- Add Subnet Masks Modal -->
<div class="modal fade" id="mdAdd">
    <div class="modal-dialog" style="max-width: 400px;">
        <div class="modal-content animated fadeInUp">
            <div class="modal-header">
            <h5 class="modal-title">Add  Subnet Mask</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                    <form  method="post" action="<?php echo base_url(); ?>SubnetMasks/Add" id="frmAdd">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Mask (e.g 32)</label>
                                    <input class="form-control" type="number" id="mask" name="mask"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Total IP Addresses</label>
                                    <input class="form-control" type="number" id="addresses" name="addresses"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Total Usable IP</label>
                                    <input class="form-control" type="number" id="usable" name="usable"/>
                                </div>
                            </div>
                        </div>

                    </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="button" class="btn btn-light" onclick="addSave()"><i class="fa fa-check-square-o"></i> Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- End Add Modal  -->

<div class="modal fade" id="mdEdit"></div>

<script>
    var tbData;
    $(document).ready(function(){
        //Validator for add form
        $("#frmAdd").validate({
                rules: {
                    mask: "required",
                    addresses: "required"
                }
        });

        var lurl = "<?php echo base_url(); ?>/SubnetMasks/get_masks_req";
        tbData = $('#tbData').DataTable({
            lengthChange: true,
            "order": [],
            "bRetrieve": true,
            "sAjaxSource": lurl,
            "aoColumns": [
                {
                    data: 'mask',
                    render: function(data){
                        return '/'+data;
                    }
                },
                { data: 'addresses' },
                { data: 'usable' },
                {
                    data: 'id',
                    render: function(data){
                        var btn = '';
                        <?php if(has_permission('subnetmasks', 'edit') ) {?>
                            btn += '<span class="tbAction"  onclick="Edit('+data+')"><i class="ti-marker-alt"></i></span>';
                        <?php } ?>
                        <?php if(has_permission('subnetmasks', 'delete') ) {?>
                            btn += '<span class="tbAction" onclick="Delete('+data+')"><i class="ti-trash"></i></span>';
                        <?php } ?>
                        return btn;
                    }
                }
            ]

        });
      });

    function addSave(){
        if(!$("#frmAdd").valid()){
            return;
        }

        //Serialize form data
        var frmData = {};
        var info = $('#frmAdd').serializeArray({checkboxesAsBools: false});
        $.each(info, function (key, input) {
            frmData[input.name] = input.value;
        });

        //Send data to server
        $("#loaderwrapper").show();
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url() ?>/SubnetMasks/save',
            data: frmData,
            success: function (response) {
                if (response == "Success") {
                    $("#loaderwrapper").hide();
                    swal("Success", 'Record added successfully', 'success');
                    tbData.ajax.reload();
                    $('#mdAdd').modal('hide');
                }
                else {
                    $("#loaderwrapper").hide();
                    swal("Status",response,"info");
                }
            },
            error: function () {
                $("#loaderwrapper").hide();
                swal("Error", 'Unable to add recored, please try again', "error");
            }
        });

    }

    function Edit(id)
    {
        $("#loaderwrapper").show();
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url(); ?>/SubnetMasks/get_edit/'+id,
            data: {},
            success: function (response) {
                $("#loaderwrapper").hide();

                $('#mdEdit').html(response);
                $('#mdEdit').modal('show');

            },
            error: function (response) {

                $("#loaderwrapper").hide();
                swal("Error", response, "error");
            }
        });
    }

      function Delete(id)
      {
          if(!confirm('Are you sure you want to delete this record, this action is not reversible?'))
          {
                return;
          }

          $("#loaderwrapper").show();
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url(); ?>/SubnetMasks/delete/'+id,
                data: {},
                success: function (response) {
                    if (response == "Success") {
                        $("#loaderwrapper").hide();
                        swal("Success", 'Record deleted successfully', 'success');
                        tbData.ajax.reload();
                    }
                    else {
                        $("#loaderwrapper").hide();
                        swal("Status",response,"info");
                    }
                },
                error: function (response) {

                    $("#loaderwrapper").hide();
                    swal("Error", response, "error");
                }
            });


      }
</script>
