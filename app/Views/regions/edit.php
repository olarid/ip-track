<div class="modal-dialog" style="max-width: 400px;">
    <div class="modal-content animated fadeInUp">
        <div class="modal-header">
        <h5 class="modal-title">Edit Region</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
                <form  method="post" action="<?php echo base_url(); ?>Regions/Edit" id="frmEdit">
                    <input type="hidden" name="edt_id" id="edt_id" value="<?php echo $region->id ?>" />
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Region Name</label>
                                <input class="form-control" type="text" id="edt_region" name="edt_region" value="<?php echo $region->region ?>"/>
                            </div>
                        </div>
                    </div>
                </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            <button type="button" class="btn btn-light" onclick="editSave()"><i class="fa fa-check-square-o"></i> Save changes</button>
        </div>
    </div>
</div>

<script>

    $(document).ready(function(){
         // Input validator
        $("#frmEdit").validate({
                rules: {
                    region: "required"
                }
        });
    });

    function editSave(){
        if(!$("#frmEdit").valid()){
            return;
        }

        //Serialize form data
        var frmData = {};
        var info = $('#frmEdit').serializeArray({checkboxesAsBools: false});

        //Remove edt_ from fields
        $.each(info, function (key, input) {
            frmData[input.name.replace('edt_', '')] = input.value;
        });

        //Send data to server
        $("#loaderwrapper").show();
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url() ?>/regions/save',
            data: frmData,
            success: function (response) {
                if (response == "Success") {
                    $("#loaderwrapper").hide();
                    swal("Success", 'Record updated successfully', 'success');
                    tbData.ajax.reload();
                    $('#mdEdit').modal('hide');
                }
                else {
                    $("#loaderwrapper").hide();
                    swal("Status",response,"info");
                }
            },
            error: function () {
                $("#loaderwrapper").hide();
                swal("Error", 'Unable to update recored, please try again', "error");
            }
        });

    }

</script>