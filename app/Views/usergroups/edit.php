
<div class="modal-dialog" style="max-width: 800px;">
    <div class="modal-content animated fadeInUp">
        <div class="modal-header">
        <h5 class="modal-title">Edit Group</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
                <form  method="post" action="<?php echo base_url(); ?>UserGroups/Edit" id="frmEdit">
                    <input type="hidden" name="edt_id" id="edt_id" value="<?php echo $group->id ?>" />
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Group Name</label>
                                <input class="form-control" type="text" id="edt_title" name="edt_title" value="<?php echo $group->title ?>"/>
                            </div>
                        </div>
                    </div>

                    <br />
                    <h5 style="text-align: center">Group Permissions</h5>
                    <hr />
                    <?php foreach($permissions as $p){ ?>
                        <h6 style="text-transform: uppercase"><?php echo $p['title'] ?></h6>
                        <div class="row">
                            <?php foreach($p['actions'] as $a){ ?>
                                <div class="col-md-3">
                                    <div class="icheck-material-white">
                                        <input type="checkbox" id="edt_<?php echo $p['slug'].'_'.$a  ?>" name="edt_<?php echo $p['slug'].'_'.$a  ?>" />
                                        <label for="edt_<?php echo $p['slug'].'_'.$a  ?>"><?php echo $a ?> </label>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <hr />
                    <?php } ?>

                </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            <button type="button" class="btn btn-light" onclick="editSave()"><i class="fa fa-check-square-o"></i> Save changes</button>
        </div>
    </div>
</div>

<script>

    $(document).ready(function(){
         // Input validator
        $("#frmEdit").validate({
                rules: {
                    title: "required"
                }
        });

        //Check existing permissions
        var permissions = <?php echo json_encode($group_permissions) ?>;

        $.each(permissions, function (key, data) {
            var elem =  document.getElementById('edt_'+data.module+'_'+data.action);
            if(elem){ elem.checked = true}
        });
    });

    function editSave(){
        if(!$("#frmEdit").valid()){
            return;
        }

        //Serialize form data
        var frmData = {};
        var info = $('#frmEdit').serializeArray({checkboxesAsBools: true});

        //Remove edt_ from fields
        $.each(info, function (key, input) {
            frmData[input.name.replace('edt_', '')] = input.value;
        });

        //Send data to server
        $("#loaderwrapper").show();
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url() ?>/UserGroups/save',
            data: frmData,
            success: function (response) {
                if (response == "Success") {
                    $("#loaderwrapper").hide();
                    swal("Success", 'Record updated successfully', 'success');
                    tbData.ajax.reload();
                    $('#mdEdit').modal('hide');
                }
                else {
                    $("#loaderwrapper").hide();
                    swal("Status",response,"info");
                }
            },
            error: function () {
                $("#loaderwrapper").hide();
                swal("Error", 'Unable to update recored, please try again', "error");
            }
        });

    }

</script>