<!-- Start BreadCrumb -->
<div class="row pt-2 pb-2">
    <div class="col-sm-12">
        <h4 class="page-title">User Groups</h4>
    </div>
</div>
<!-- End BreadCrumb -->

<!-- Begin Table -->
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <strong class=" text-uppercase">Manage Groups</strong>
                <?php if(has_permission('usergroups', 'add') ) {?>
                    <button class="btn btn-light card-btn" onclick="$('#mdAdd').modal('show');">Add</button>
                <?php } ?>
            </div>
            <div class="card-body">
                <div class="table-responsive">

                    <table id="tbData" class="table table-bordered" style="width: 100%">
                        <thead>
                            <tr>
                                <th>Group Name</th>
                                <th>Date Created</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<!--End Table-->

<!-- Add Group Modal -->
<div class="modal fade" id="mdAdd">
    <div class="modal-dialog" style="max-width: 800px;">
        <div class="modal-content animated fadeInUp">
            <div class="modal-header">
            <h5 class="modal-title">Add Group</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                    <form  method="post" action="<?php echo base_url(); ?>UserGroups/Add" id="frmAdd">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Group Name</label>
                                    <input class="form-control" type="text" id="title" name="title"/>
                                </div>
                            </div>
                        </div>
                        <br />
                        <h5 style="text-align: center">Group Permissions</h5>
                        <hr />
                        <?php foreach($permissions as $p){ ?>
                            <h6 style="text-transform: uppercase"><?php echo $p['title'] ?></h6>
                            <div class="row">
                                <?php foreach($p['actions'] as $a){ ?>
                                    <div class="col-md-3">
                                        <div class="icheck-material-white">
                                            <input type="checkbox" id="<?php echo $p['slug'].'_'.$a  ?>" name="<?php echo $p['slug'].'_'.$a  ?>" />
                                            <label for="<?php echo $p['slug'].'_'.$a  ?>"><?php echo $a ?> </label>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <hr />
                        <?php } ?>
                    </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="button" class="btn btn-light" onclick="addSave()"><i class="fa fa-check-square-o"></i> Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- End Add Modal  -->

<div class="modal fade" id="mdEdit"></div>

<script>
    var tbData;
    $(document).ready(function(){
        //Validator for add form
        $("#frmAdd").validate({
                rules: {
                    title: "required"
                }
        });

        var lurl = "<?php echo base_url(); ?>/UserGroups/get_groups_req";
        tbData = $('#tbData').DataTable({
            lengthChange: true,
            "bRetrieve": true,
            "sAjaxSource": lurl,
            "aoColumns": [
                { data: 'title' },
                {
                    data: 'created_at',
                    render: function(data){
                        return moment(data).format('MM/DD/YYYY');
                    }
                },
                {
                    data: 'id',
                    render: function(data){
                        var btn = '';
                        <?php if(has_permission('usergroups', 'edit') ) {?>
                            btn += '<span class="tbAction"  onclick="Edit('+data+')"><i class="ti-marker-alt"></i></span>';
                        <?php } ?>
                        <?php if(has_permission('usergroups', 'delete') ) {?>
                            btn += '<span class="tbAction" onclick="Delete('+data+')"><i class="ti-trash"></i></span>';
                        <?php } ?>
                        return btn;
                    }
                }
            ]

        });
      });

    function addSave(){
        if(!$("#frmAdd").valid()){
            return;
        }

        //Serialize form data
        var frmData = {};
        var info = $('#frmAdd').serializeArray({checkboxesAsBools: true});
        $.each(info, function (key, input) {
            frmData[input.name] = input.value;
        });

        //Send data to server
        $("#loaderwrapper").show();
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url() ?>/UserGroups/save',
            data: frmData,
            success: function (response) {
                if (response == "Success") {
                    $("#loaderwrapper").hide();
                    swal("Success", 'Record added successfully', 'success');
                    tbData.ajax.reload();
                    $('#mdAdd').modal('hide');
                }
                else {
                    $("#loaderwrapper").hide();
                    swal("Status",response,"info");
                }
            },
            error: function () {
                $("#loaderwrapper").hide();
                swal("Error", 'Unable to add recored, please try again', "error");
            }
        });

    }

    function Edit(id)
    {
        $("#loaderwrapper").show();
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url(); ?>/UserGroups/get_edit/'+id,
            data: {},
            success: function (response) {
                $("#loaderwrapper").hide();

                $('#mdEdit').html(response);
                $('#mdEdit').modal('show');

            },
            error: function (response) {

                $("#loaderwrapper").hide();
                swal("Error", response, "error");
            }
        });
    }

      function Delete(id)
      {
          if(!confirm('Are you sure you want to delete this record, this action is not reversible?'))
          {
                return;
          }

          $("#loaderwrapper").show();
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url(); ?>/UserGroups/delete/'+id,
                data: {},
                success: function (response) {
                    if (response == "Success") {
                        $("#loaderwrapper").hide();
                        swal("Success", 'Record deleted successfully', 'success');
                        tbData.ajax.reload();
                    }
                    else {
                        $("#loaderwrapper").hide();
                        swal("Status",response,"info");
                    }
                },
                error: function (response) {

                    $("#loaderwrapper").hide();
                    swal("Error", response, "error");
                }
            });


      }
</script>
