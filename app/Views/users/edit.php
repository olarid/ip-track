<div class="modal-dialog" style="max-width: 600px;">
    <div class="modal-content animated fadeInUp">
        <div class="modal-header">
        <h5 class="modal-title">Edit User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
            <?php echo form_open_multipart('users/save','id="frmEdit"');?>

            <input type="hidden" name="edt_id" id="edt_id" value="<?php echo $user->id ?>" />
            <input type="hidden" name="edt_profile_pic" id="edt_profile_pic" value="<?php echo $user->profile_pic ?>" />
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label">Full Name</label>
                        <input class="form-control" type="text" id="edt_name" name="edt_name" value="<?php echo $user->name ?>"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label">Email</label>
                        <input class="form-control" type="email" id="edt_email" name="edt_email" value="<?php echo $user->email ?>"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label">Password</label>
                        <input class="form-control" type="password" id="edt_password" name="edt_password" value="<?php echo $user->password ?>"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label">User Group</label>
                        <select class="selectpicker form-control"  name="edt_user_group" id="edt_user_group"><?php echo $groups ?></select>
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label">Profile Picture</label> <br />
                        <?php if(isset($user->profile_pic)){ ?>
                            <img src="<?php echo base_url(); ?>/assets/uploads/profile-pictures/<?php echo $user->profile_pic ?>" style="max-height: 100px;" />
                            <br />
                        <?php } ?>
                        <input type="file" accept=".jpg,.jpeg,.png,.gif" name="edt_image_upload" id="edt_image_upload" />
                    </div>
                </div>
            </div>

            <?php echo form_close() ?>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
        <button type="button" class="btn btn-light" onclick="editSave()"><i class="fa fa-check-square-o"></i> Save changes</button>
        </div>
    </div>
</div>

<script>

    $(document).ready(function(){
         // validate signup form on keyup and submit
        $("#frmEdit").validate({
            rules: {
                name: "required",
                email: {
                    required: true,
                    email: true
                },
                password: "required"
            }
        });

        $('.selectpicker').selectpicker();
    });

    function editSave()
    {
        if(!$("#frmEdit").valid()){
            return;
        }

        //Add files and manaullay add form data
        var frmData = new FormData();
        var files = $("#edt_image_upload").get(0).files;

        //Add file to form data
        frmData.append('image_upload', files[0]);

        var other_data = $('#frmEdit').serializeArray({checkboxesAsBools: false});
        $.each(other_data, function (key, input) {
            frmData.append(input.name.replace('edt_', ''), input.value);
        });

        //Send data to server
        $("#loaderwrapper").show();
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url() ?>/users/save',
            data: frmData,
            processData: false,
            contentType: false,
            enctype: 'multipart/form-data',
            success: function (response) {
                if (response == "Success") {
                    $("#loaderwrapper").hide();
                    swal("Success", 'User updated successfully', 'success');
                    tbData.ajax.reload();
                    $('#mdEdit').modal('hide');
                }
                else {
                    $("#loaderwrapper").hide();
                    swal("Status",response,"info");
                }
            },
            error: function () {
                $("#loaderwrapper").hide();
                swal("Error", 'Unable to update user, please try again', "error");
            }
        });
    }
</script>