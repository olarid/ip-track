<!-- Start BreadCrumb -->
<div class="row pt-2 pb-2">
    <div class="col-sm-12">
        <h4 class="page-title">Users</h4>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url()?>Users">Users</a></li>
            <li class="breadcrumb-item active"><a href="javaScript:void();">All Users</a></li>
        </ol>
    </div>
</div>
<!-- End BreadCrumb -->

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header text-uppercase">
                All Users
                <?php if(has_permission('users', 'add') ) {?>
                    <button class="btn btn-light card-btn" onclick="$('#mdAdd').modal('show');">Add</button>
                <?php } ?>
            </div>
            <div class="card-body">
                <div class="table-responsive">

                    <table id="tbData" class="table table-bordered" style="width: 100%">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>User Group</th>
                                <th>Date Created</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div><!--End Row-->


<div class="modal fade" id="mdAdd">
    <div class="modal-dialog" style="max-width: 500px;">
        <div class="modal-content animated fadeInUp">
            <div class="modal-header">
            <h5 class="modal-title">Add User</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <?php echo form_open_multipart('Users/Add','id="frmAdd"');?>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">Full Name</label>
                            <input class="form-control" type="text" id="name" name="name"/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">Email</label>
                            <input class="form-control" type="email" id="email" name="email"/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">Password</label>
                            <input class="form-control" type="password" id="password" name="password"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">User Group</label>
                            <select class="selectpicker form-control"  name="user_group" id="user_group"><?php echo $groups ?></select>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">Profile Picture</label> <br />
                            <input type="file" accept=".jpg,.jpeg,.png,.gif" name="image_upload" id="image_upload" />
                        </div>
                    </div>
                </div>

                <?php echo form_close() ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="button" class="btn btn-light" onclick="addSave()"><i class="icon-user"></i> Add User</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="mdEdit"></div>
<script>
    var tbData;
    $(document).ready(function() {

        $("#frmAdd").validate({
                rules: {
                    name: "required",
                    email: {
                        required: true,
                        email: true
                    },
                    password: "required"
                }
        });

        var lurl = "<?php echo base_url(); ?>/users/get_users_req";
        tbData = $('#tbData').DataTable({
            lengthChange: true,
            "bRetrieve": true,
            "sAjaxSource": lurl,
            "aoColumns": [
                { data: 'name' },
                { data: 'email'},
                { data: 'group_name'},
                {
                    data: 'created_at',
                    render: function(data){
                        return moment(data).format('MM/DD/YYYY');
                    }
                },
                {
                    data: 'id',
                    render: function(data){
                        var btn = '';
                        <?php if(has_permission('users', 'edit') ) {?>
                            btn += '<span class="tbAction"  onclick="Edit('+data+')"><i class="ti-marker-alt"></i></span>';
                        <?php } ?>
                        <?php if(has_permission('users', 'delete') ) {?>
                            btn += '<span class="tbAction" onclick="Delete('+data+')"><i class="ti-trash"></i></span>';
                        <?php } ?>
                        return btn;
                    }
                }
            ]

        });

    });


    function addSave(){
        if(!$("#frmAdd").valid()){
            return;
        }


        //Add files and manaullay add form data
        var frmData = new FormData();
        var files = $("#image_upload").get(0).files;

        //Add file to form data
        frmData.append('image_upload', files[0]);

        var other_data = $('#frmAdd').serializeArray({checkboxesAsBools: false});
        $.each(other_data, function (key, input) {
            frmData.append(input.name, input.value);
        });

        //add group name
        frmData.append('group_name', $('#user_group :selected').data('name'));

        //Send data to server
        $("#loaderwrapper").show();
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url() ?>/users/save',
            data: frmData,
            processData: false,
            contentType: false,
            enctype: 'multipart/form-data',
            success: function (response) {
                if (response == "Success") {
                    $("#loaderwrapper").hide();
                    swal("Success", 'User added successfully', 'success');
                    tbData.ajax.reload();
                    $('#mdAdd').modal('hide');
                }
                else {
                    $("#loaderwrapper").hide();
                    swal("Status",response,"info");
                }
            },
            error: function () {
                $("#loaderwrapper").hide();
                swal("Error", 'Unable to add user, please try again', "error");
            }
        });
    }

    function Edit(id)
    {
        $("#loaderwrapper").show();
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url(); ?>/users/get_edit/'+id,
            data: {},
            success: function (response) {
                $("#loaderwrapper").hide();

                $('#mdEdit').html(response);
                $('#mdEdit').modal('show');

            },
            error: function (response) {

                $("#loaderwrapper").hide();
                swal("Error", response, "error");
            }
        });
    }

    function Delete(id)
    {
        if(!confirm('Are you sure you want to delete this user, this action is not reversible?'))
        {
            return;
        }

        $("#loaderwrapper").show();
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url(); ?>/users/delete/'+id,
            data: {},
            success: function (response) {
                $("#loaderwrapper").hide();
                tbData.ajax.reload();
                swal("Status", response, "info");

            },
            error: function (response) {

                $("#loaderwrapper").hide();
                swal("Error", response, "error");
            }
        });


    }
</script>
