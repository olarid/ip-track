<div class="modal-dialog" style="max-width: 600px;">
    <div class="modal-content animated fadeInUp">
        <div class="modal-header">
        <h5 class="modal-title">Edit Vendor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
                <form  method="post" action="<?php echo base_url(); ?>Vendors/Edit" id="frmEdit">
                    <input type="hidden" name="edt_id" id="edt_id" value="<?php echo $vendor->id ?>" />
                    <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Full Name</label>
                                    <input class="form-control" type="text" id="edt_name" name="edt_name" value="<?php echo $vendor->name ?>" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Email</label>
                                    <input class="form-control" type="email" id="edt_email" name="edt_email" value="<?php echo $vendor->email ?>" />
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">IP (e.g. xxx.xxx.xxx.xxx)</label>
                                    <input class="form-control" type="text" id="edt_ip" name="edt_ip" placeholder = " (e.g. xxx.xxx.xxx.xxx)" value="<?php echo $vendor->ip ?>" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Subnet Mask</label>
                                    <select class="selectpicker form-control"  name="edt_mask" id="edt_mask"><?php echo $masks ?></select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Location</label>
                                    <select class="selectpicker form-control "  name="edt_location" id="edt_location"><?php echo $locations ?></select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">RIR</label>
                                    <select class="selectpicker form-control "  name="edt_rir" id="edt_rir"><?php echo $regions ?></select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Wholesale Price</label>
                                    <input type="number" class="form-control" name="edt_wholesale_price" id = "edt_wholesale_price" value="<?php echo $vendor->wholesale_price ?>"  />
                                </div>
                            </div>
                            <div class="col-md-6">
                            </div>
                        </div>
                </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            <button type="button" class="btn btn-light" onclick="editSave()"><i class="fa fa-check-square-o"></i> Save changes</button>
        </div>
    </div>
</div>

<script>

    $(document).ready(function(){
         // Input validator
        $("#frmEdit").validate({
                rules: {
                    name: "required",
                    ip:  {
                        required: true,
                        IP4Checker: true
                    },
                    mask: "required",
                    wholesale_price: "required"
                }
        });

        $('.selectpicker').selectpicker();
    });

    function editSave(){
        if(!$("#frmEdit").valid()){
            return;
        }

        //Serialize form data
        var frmData = {};
        var info = $('#frmEdit').serializeArray({checkboxesAsBools: false});

        //Remove edt_ from fields
        $.each(info, function (key, input) {
            frmData[input.name.replace('edt_', '')] = input.value;
        });

        //Send data to server
        $("#loaderwrapper").show();
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url() ?>/vendors/save',
            data: frmData,
            success: function (response) {
                if (response == "Success") {
                    $("#loaderwrapper").hide();
                    swal("Success", 'Record updated successfully', 'success');
                    tbData.ajax.reload();
                    $('#mdEdit').modal('hide');
                }
                else {
                    $("#loaderwrapper").hide();
                    swal("Status",response,"info");
                }
            },
            error: function () {
                $("#loaderwrapper").hide();
                swal("Error", 'Unable to update recored, please try again', "error");
            }
        });

    }

</script>