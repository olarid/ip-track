<!-- Start BreadCrumb -->
<div class="row pt-2 pb-2">
    <div class="col-sm-12">
        <h4 class="page-title">Vendors</h4>
    </div>
</div>
<!-- End BreadCrumb -->

<!-- Begin Table -->
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <strong class=" text-uppercase">Manage Vendors</strong>
                <?php if(has_permission('vendors', 'add') ) {?>
                    <button class="btn btn-light card-btn" onclick="$('#mdAdd').modal('show');">Add</button>
                <?php } ?>
            </div>
            <div class="card-body">
                <div class="table-responsive">

                    <table id="tbData" class="table table-bordered" style="width: 100%">
                        <thead>
                            <tr>
                                <th>Vendor Name</th>
                                <th>Email</th>
                                <th>IP Address</th>
                                <th>RIR</th>
                                <th>Wholesale Price</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<!--End Table-->

<!-- Add Vendor Modal -->
<div class="modal fade" id="mdAdd">
    <div class="modal-dialog" style="max-width: 600px;">
        <div class="modal-content animated fadeInUp">
            <div class="modal-header">
            <h5 class="modal-title">Add Vendor</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                    <form  method="post" action="<?php echo base_url(); ?>Vendors/Add" id="frmAdd">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Full Name</label>
                                    <input class="form-control" type="text" id="name" name="name"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Email</label>
                                    <input class="form-control" type="email" id="email" name="email"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">IP (e.g. xxx.xxx.xxx.xxx)</label>
                                    <input class="form-control" type="text" id="ip" name="ip" placeholder = " (e.g. xxx.xxx.xxx.xxx)"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Subnet Mask</label>
                                    <select class="selectpicker form-control"  name="mask" id="mask"><?php echo $masks ?></select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Location</label>
                                    <select class="selectpicker form-control "  name="location" id="location"><?php echo $locations ?></select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">RIR</label>
                                    <select class="selectpicker form-control "  name="rir" id="rir"><?php echo $regions ?></select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Wholesale Price</label>
                                    <input type="number" class="form-control" name="wholesale_price" id = "wholesale_price" />
                                </div>
                            </div>
                            <div class="col-md-6">
                            </div>
                        </div>

                    </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="button" class="btn btn-light" onclick="addSave()"><i class="fa fa-check-square-o"></i> Save </button>
            </div>
        </div>
    </div>
</div>
<!-- End Add Modal  -->

<div class="modal fade" id="mdEdit"></div>

<script>
    var tbData;
    $(document).ready(function(){
        //Validator for add form
        $("#frmAdd").validate({
                rules: {
                    name: "required",
                    ip:  {
                        required: true,
                        IP4Checker: true
                    },
                    mask: "required",
                    wholesale_price: "required"
                }
        });

        var lurl = "<?php echo base_url(); ?>/vendors/get_vendors_req";
        tbData = $('#tbData').DataTable({
            lengthChange: true,
            "bRetrieve": true,
            "sAjaxSource": lurl,
            "aoColumns": [
                { data: 'name' },
                { data: 'email' },
                {
                    data: 'ip',
                    render: function(data, type, row){
                        return data + '/' + row['mask'];
                    }
                },
                { data: 'rir' },
                { data: 'wholesale_price' },
                {
                    data: 'id',
                    render: function(data){
                        var btn = '';
                        <?php if(has_permission('vendors', 'edit') ) {?>
                            btn += '<span class="tbAction"  onclick="Edit('+data+')"><i class="ti-marker-alt"></i></span>';
                        <?php } ?>
                        <?php if(has_permission('vendors', 'delete') ) {?>
                            btn += '<span class="tbAction" onclick="Delete('+data+')"><i class="ti-trash"></i></span>';
                        <?php } ?>
                        return btn;
                    }
                }
            ]

        });
      });

    function addSave(){
        if(!$("#frmAdd").valid()){
            return;
        }

        //Serialize form data
        var frmData = {};
        var info = $('#frmAdd').serializeArray({checkboxesAsBools: false});
        $.each(info, function (key, input) {
            frmData[input.name] = input.value;
        });
        //Send data to server
        $("#loaderwrapper").show();
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url() ?>/vendors/save',
            data: frmData,
            success: function (response) {
                if (response == "Success") {
                    $("#loaderwrapper").hide();
                    swal("Success", 'Record added successfully', 'success');
                    tbData.ajax.reload();
                    $('#mdAdd').modal('hide');
                }
                else {
                    $("#loaderwrapper").hide();
                    swal("Status",response,"info");
                }
            },
            error: function () {
                $("#loaderwrapper").hide();
                swal("Error", 'Unable to add recored, please try again', "error");
            }
        });

    }

    function Edit(id)
    {
        $("#loaderwrapper").show();
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url(); ?>/vendors/get_edit/'+id,
            data: {},
            success: function (response) {
                $("#loaderwrapper").hide();

                $('#mdEdit').html(response);
                $('#mdEdit').modal('show');

            },
            error: function (response) {

                $("#loaderwrapper").hide();
                swal("Error", response, "error");
            }
        });
    }

      function Delete(id)
      {
          if(!confirm('Are you sure you want to delete this record, this action is not reversible?'))
          {
                return;
          }

          $("#loaderwrapper").show();
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url(); ?>/vendors/delete/'+id,
                data: {},
                success: function (response) {
                    if (response == "Success") {
                        $("#loaderwrapper").hide();
                        swal("Success", 'Record deleted successfully', 'success');
                        tbData.ajax.reload();
                    }
                    else {
                        $("#loaderwrapper").hide();
                        swal("Status",response,"info");
                    }
                },
                error: function (response) {

                    $("#loaderwrapper").hide();
                    swal("Error", response, "error");
                }
            });


      }
</script>
